# Deploy to LKE Cluster from Jenkins Pipeline 2024

### Pre-Requisites

Linode

eks command line

git

Jenkins


#### Project Outline

In the project we will create a k8s cluster on linode and deploy using from Jenkins Pipleine
We will also see the reduced use of platform authentication
We will create a k8s cluster on Linode Kubernetes Engine and use kubeconfig to connect and authenticate
Lets get started
From the previous project, we already have kubectl inside Jenkins via the docker container

[Reference to lke branch](https://gitlab.com/FM1995/cd-deploy-to-eks-cluster-from-jenkins-pipeline/-/blob/deploy-to-lke)




#### Getting started

From the previous project, we already have kubectl inside Jenkins via the docker container

[Link to kubectl setup inside Jenkins](https://gitlab.com/FM1995/deploy-to-eks-cluster-from-jenkins-pipeline-2024)

We will also need a kubernetes CLI Jenkins Plugin so that we can execute kubectl with kubeconfig credentials

We can then configure Jenkinsfile to deploy to LKE cluster

Lets create Kubernetes cluster on Linode and connect to it

![Image1](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image1.png)


![Image2](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image2.png)

Now created

![Image3](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image3.png)

Running the below command

```
export KUBECONFIG=~/Downloads/new-test-kubeconfig.yaml
```

Running the below

```
kubectl get nodes
```

![Image4](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image4.png)

And matches the linode in the above

![Image5](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image5.png)


So have now successfully connected

Now time to add the LKE credentials in Jenkins

Using the kubeconfig file as a secret

![Image6](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image6.png)

Successfully created the credentials

![Credentials Image](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/credentials.png)

Next we can install the plugin to connect the kubectl cluster to Jenkins ‘kubernetes cli’ this will allow us to use the kubeconfig file used in the above credentials and connect to the cluster

![Image7](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image7.png)



Installed the plugin

![Image8](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image8.png)


Again taking a basic jenkinsfile

![Image9](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image9.png)



Making the necessary changes to the Jenkinsfile

![Image10](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image10.png)


And push the jenkinsfile

Now ready for build

And can see it is successful

![Image11](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image11.png)


Checking the pod can see it is up and running

![Image12](https://gitlab.com/FM1995/deploy-to-lke-cluster-from-jenkins-pipeline-2024/-/raw/main/Images/Image12.png)

[Finalised Jenkinsfile](https://gitlab.com/FM1995/cd-deploy-to-eks-cluster-from-jenkins-pipeline/-/blob/deploy-to-lke/Jenkinsfile?ref_type=heads)









